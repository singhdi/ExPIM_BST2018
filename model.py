"""Created on 21.01.2018 @author: dipali_singh and oliver_ebenhoeh"""

'''Extended Phosphate Interaction Model (ExPIM)'''

'''parameter values for experiment 9a P-uptake by hungry algae from full P medium -> growth limitation due to light limitation lag phase incorporated to the model'''
import scipy
import scipy.optimize as so
import scipy.integrate as sp
import numpy as np
import matplotlib.pyplot as pl
import csv

'''
parameter  values
'''
upMax = 0.025   # maximum rate of algal P-uptake
cMax = 3.0 # maximum growth rate on P-ortho in algae
cPMax = 0.08  # maximum usage rate of P-ortho by algae
immoMax = 0.5  # maximum immobilisation rate
remoMax = 0.2  # maximum remobilisation rate
constMax = 1.0  # maximum structure building rate


Kex = 0.5  # half saturation value P-uptake
Kimmo = 0.5  # half saturation value immobilisation
Kremo = 0.5  # half saturation value remobilisation
Kstruc = 0.0001  # half saturation value for structure building

K = 0.01  # help parameter, so that a term cannot go zero
Hpin = 0.01  # half saturation value for algal P-usage/ growth
MaxOrtho = 0.2  # maximum capacity for orthoP in the algal cell
MaxPoly = 0.5  # maximum capacity for polyP
MaxOrg = 1.0  # maximum capacity for Po


# light limitation parameter
Lex = 250.0  # provided light intensity
A0 = 0.5
r = 1.9
S = 0.1
I0 = 1.0
z = 0.05
Iopt = 250.
beta = 0.1

cv = 0.5 #testing cell volume to reactor volume ratio
m = 1.0  # exponent for structure building
Gmax = 1.0
Kgfact = 0.5
gfactMax = 0.8
Hpg=0.08
QoP = 0.003




# initial conditions and simulation time

X0 = [0.5, 0.0002, 0.00001, 0.0008, 2.0,0.001]  # inital values (C0, Portho0, Ppoly0, Po0, Pex0,G0)

tend = 96
trange = np.linspace(0, tend, 100*tend)


def uptake(Pex, Portho):
    '''
    :param Pex: external P
    :param Portho: orthophosphate
    :param Ppoly: polyphosphate
    :param Biomass: algal biomass
    :return: uptake of external P into the orthoP pool limited by a orthoP
    '''
    return upMax * Pex/(Pex+Kex) * (1 - Portho/MaxOrtho)/(1 - Portho/MaxOrtho + K)

def gmachinery(G0,Po):
    '''
    :param G0:initial amount of machinery available
    :return: rate of machinery synthesis
    '''
	
    return gfactMax*G0/(G0+Kgfact)*(1-G0/Gmax) * (Po-QoP) / (Hpg + Po-QoP)

def growthrate(Cmax, Po, Biomass, L,G0):
    '''
    :param Cmax: maximum transformation/ growth rate
    :param Portho: orthophosphate
    :param Po: organic phosphate
    :param Biomass: algal biomass
    :param L: provided external ligth intensity
    :return: transformation of orthoP or growth of the algal biomass
    inhibited at low Po levels and influenced by light
    '''
    return Cmax* (Po-QoP) / (Hpin + Po-QoP)  * lightAll(L, Biomass) * G0



def immoblisation(Portho, Ppoly , dBiomass):
    '''
    :param Portho: orthophosphate
    :param Ppoly: polyphosphate
    :return: immobilisation of orthoP into polyP limited by a polyP capacity only when no growth is occurring
    '''
    return immoMax * Portho**1/(Portho**1 + Kimmo**1) * (1 - Ppoly/MaxPoly)/(1 - Ppoly/MaxPoly + K) * (1 - dBiomass/(dBiomass+K))


def remobilisation(Ppoly, Portho):
    '''
    :param Ppoly: polyphosphate
    :param Portho: orthophosphate
    :return: remobilisation of polyP into orthoP limited by the orthoP capacity
    '''
    return remoMax * Ppoly/(Ppoly + Kremo) * (1 - Portho/MaxOrtho)/(1 - Portho/MaxOrtho + K)


def construction(Portho, Po):
    '''
    :param Portho: orthophosphate
    :param Po: organic phosphate
    :return: structure building of organic Phosphate using orthoP and organic P
    '''
    return constMax * Portho/(Portho + Kstruc) * Po * (1. - Po/MaxOrg)/(1. - Po/MaxOrg + K)



def LG(L):
    '''
    :param L: light intensity
    after Dauta et al (1990)
    :return: influence of light intensity on growth rate
    '''
    Ir = L/Iopt
    return 2.*(1.+beta)*Ir/(Ir**2.+2.*beta*Ir+1.)


def lightLim(L,Biomass):
    '''
    :param Biomass: biomass concentration
    after Bojan Tamburic (Sydney)
    :return: influence of biomass concentration on light intensity within the tube
    '''
    A = A0*np.exp(r*Biomass)
    a = A+S
    return L*np.exp(-a*z)


def lightAll(L, Biomass):
    '''
    :param L: provided light intensity
    :param Biomass: algal biomass
    :return: combined influence of biomass on internal light intensity and
    of the internal light intensity on biomass growth
    '''
    return LG(lightLim(L,Biomass))


def model(X0, t):
    '''
    :param X0: initial conditions
    :param t: time
    :return: ODE system
    '''
    (Biomass, Portho, Ppoly, Po, Pex,G0) = X0
    dBiomass = growthrate(cMax, Po, Biomass, Lex,G0)*Biomass
    
    mu = growthrate(cMax, Po, Biomass, Lex,G0)
    dPortho = uptake(Pex, Portho) - immoblisation(Portho, Ppoly, dBiomass) + remobilisation(Ppoly, Portho) - construction(Portho, Po) - mu*Portho
    dPpoly = immoblisation(Portho, Ppoly, dBiomass) - remobilisation(Ppoly, Portho)- mu*Ppoly
    dPo = construction(Portho, Po) - gmachinery(G0,Po)- mu*Po
    dPex = - uptake(Pex, Portho)*Biomass* cv
    dG = gmachinery(G0,Po)-mu*G0
    l.append([t,growthrate(cMax, Po, Biomass, Lex,G0)*Biomass])
    return [dBiomass, dPortho, dPpoly, dPo, dPex,dG]

v = scipy.zeros((6),'d')
def rate_eqs(X0):
    (Biomass, Portho, Ppoly, Po, Pex, G0) = X0
    v[0] = growthrate(cMax, Po, Biomass, Lex, G0)
    v[1] = immoblisation(Portho, Ppoly, Biomass)
    v[2] = remobilisation(Ppoly, Portho)
    v[3] = construction(Portho, Po)
    v[4] = uptake(Pex, Portho)
    v[5] = gmachinery(G0,Po)
    return v


def Rates(s):
	vv = list()
	for i in range(len(s)):
		rate = rate_eqs(s[i])
		vv.append([rate[0],rate[1],rate[2],rate[3],rate[4],rate[5],rate[5]])
	return np.array(vv)

'''
import of the experimental data
'''
def csvImport(filename):
    '''
    :param filename: file with data
    :return: data in a list
    '''
    file = open(filename, 'r')
    reader = csv.reader(file)
    rows = list()
    l = list()
    for row in reader:
        rows.append(row)

    for i in range(len(rows)):
        l.append([float(rows[i][0]), float(rows[i][1])])

    return l


# calculation of the growth rate according to the calculation in the experiments

def growthRate(s,trange):
    '''
    :param s: simulation results
    :return: list with values for growth rates and time points
    '''
    g = list()
    trangeg = list()
    irange = range(len(s))
    for i in irange:
        a = 0
        b = 0
        if i > 4 and i < (len(irange)-4):
            for k in range(0,5):
	    	a += np.log(s[i+k][0])
            	b += np.log(s[i-k-1][0])		
            h = ((a/5.0) - (b/5.0))/(trange[i+2] - trange[i-3])
            g.append(h)
            trangeg.append(trange[i])
    return g, trangeg



'''
simulation
'''
l = list()
s = sp.odeint(model, X0, trange)
l = np.array(l)
vv = Rates(s)


# experimental data
PexData = np.array(csvImport('Puptake.csv'))
ODdata = np.array(csvImport('OD680.csv'))
GrowthRateData = np.array(csvImport('GrowthRate.csv'))

gr, trangegr = growthRate(s,trange)

#fin_s = scipy.copy(s[-1,:])
#ss = so.fsolve(model,fin_s,args=None, xtol=1e-08)

print 'last state of integration as initial value for Steady State Concentration'
#print fin_s

print 'Steady State Concentration'
#print ss

print 'Steady-state fluxes'
#print rate_eqs(ss)


'''
Plots
'''

def Plot_Biomass(s,ODdata,trange):
	pl.xticks(fontsize=26)
	pl.yticks(fontsize=26)
	pl.plot(ODdata[:, 0], ODdata[:, 1], 'rs', mew=5, label='OD data')
	pl.plot(trange, s[:, 0], color='magenta', label='Simulation', linewidth=10)
	pl.legend(loc='best')
	pl.xlabel('time [h]', fontsize=30)
	pl.ylabel('Biomass [OD]', fontsize=30)
	pl.title('Biomass', fontsize=40)
	pl.axis([0, tend, 0, 4.0])
	pl.show()

def Plot_PUptake(s,PexData,trange):
	pl.xticks(fontsize=26)
	pl.yticks(fontsize=26)
	pl.plot(PexData[:, 0], PexData[:, 1], 'rs', mew=5, label='medium P data')
	pl.plot(trange, s[:, 4], color='magenta', label='Simulation', linewidth=10)	
	pl.legend(loc='best')
	pl.xlabel('time [h]', fontsize=30)
	pl.ylabel('Phosphate [mM]', fontsize=30)
	pl.title('Phosphate Uptake', fontsize=40)
	pl.axis([0, tend, 0, 2.5])
	pl.show()


def Plot_PPool(s,v, trange,trangegr,gr):
	pl.xticks(fontsize=26)
	pl.yticks(fontsize=26)
	pl.plot(trange, s[:, 1], color='navy', label='SIP', linewidth=10)
	pl.plot(trange, s[:, 2], color='lime', label='Poly-P', linewidth=10)
	pl.plot(trange, s[:, 3], color='goldenrod', label='SOP', linewidth=10)
	pl.legend(loc='best')
	pl.xlabel('time [h]', fontsize=30)
	pl.ylabel('P/biomass [mg/g dw]', fontsize=30)
	pl.title('Algal Phosphate Pool', fontsize=40)
	pl.show()

def Plot_Growth(GrowthRateData,l,trangegr,gr):
	pl.xticks(fontsize=26)
	pl.yticks(fontsize=26)
	pl.plot(GrowthRateData[:, 0], GrowthRateData[:, 1], 'rs',mew=5, label='growth rate data')
	pl.plot(trangegr, gr, color="magenta", label="Simulation", linewidth=10)
	pl.legend(loc='best')
	pl.xlabel('time [h]', fontsize=30)
	pl.ylabel('growth rate [OD/h]', fontsize=30)
	pl.title('Growth Rate', fontsize=40)
	pl.axis([0, tend, 0.0, 0.25])
	pl.show()


Plot_Biomass(s,ODdata,trange)
Plot_PUptake(s,PexData,trange)
Plot_Growth(GrowthRateData,l,trangegr,gr)
Plot_PPool(s,vv,trange,trangegr,gr)

